<?php
namespace EESC_Theme\Controllers;

use CodeIgniter\Controller;

class Auth extends Controller
{
    public function login()
    {
        \CodeIgniter\Events\Events::trigger('login');
        return view('EESC_Theme\Auth\login');
    }

    public function logout()
    {
        \CodeIgniter\Events\Events::trigger('logout');
        session()->destroy();
        return redirect()->to(base_url());
    }
}
