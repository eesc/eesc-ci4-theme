<?php namespace Config;

use CodeIgniter\Config\BaseConfig;

class Eesc_ci4_theme extends BaseConfig
{
    public $base = ''; # definido no construtor
    public $title = 'TPL';
    public $appShortName = 'TPL';
    public $appName = 'Template Demo';

    public $unidadeUSP = [
        'nome' => 'Escola de Engenharia de São Carlos',
        'url' => 'https://www.eesc.usp.br',
        'logo' => 'assets/eesc-ci4-theme/img/logo_eesc_horizontal.png',
        'sistemas_url' => 'https://sistemas.eesc.usp.br',
        'sistemas_menu' => 'Sistemas EESC',
    ];

    public $user = [
        'nome' => 'Usuário demo',
        'foto' => '',
        'codpes' => '123456',
        'email' => 'email@usp.br',
        'ramal' => '3373 0000',
    ];

    public $auth = [
        'login_url' => 'loginusp',
        'logout_url' => 'logout',
        'logout_method' => 'get',
    ];

    public $controlSidebar = true;

    public $topbar = [
        'left' => [
            [
                'nome' => 'Menu 1',
                'url' => 'menu1',
            ],
            [
                'nome' => 'Menu 2',
                'url' => 'menu2',
            ],
        ],
        'right' => [
            [
                'nome' => '<i class="fas fa-cog"></i>',
                'url' => '#',
                'title' => 'Configurações',
            ],
        ],
    ];
    public function __construct()
    {
        $this->base = base_url();

        $this->user = '';

        /* Todas as variáveis publicas podem ser sobrescritas aqui ou pelo .env

        no .env
        eesc_ci4_theme.appName = 'Sistema de atendimento'
        eesc_ci4_theme.appShortName = 'Atendimento'
        eesc_ci4_theme.title = 'Atendimento'

        Aqui
        $this->title = 'Titulo';

        # Ativa ou desativa o sidebar da direita, section('control-sidebar')
        # $this->controlSidebar = true;

        # Configurar o topbar da aplicação.
        $this->topbar = [];

         */

        parent::__construct();
    }
}
