<?php

#Modules that define a “current user” should define the function user_id() to return the user’s unique identifier, or null for “no current user”

$theme = json_decode(json_encode(config('Eesc_ci4_theme')), true);
if ($user = session('user')) {
    $theme['user'] = [
        'nome' => session()->usuario_sistemas_eesc->getNome(),
        'foto' => '',
        #'foto' => session()->usuario_sistemas_eesc->getFoto(),
        'codpes' => session()->usuario_sistemas_eesc->getCodigoPessoa(),
        'email' => session()->usuario_sistemas_eesc->getEmail(),
        'ramal' => session()->usuario_sistemas_eesc->getRamal(),
    ];
}
?>
<!DOCTYPE html>

<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=$theme['title']?></title>
    <base href="<?=$theme['base']?>/">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="assets/adminlte/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="assets/adminlte/dist/css/adminlte.min.css">

    <link rel="stylesheet" href="assets/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">

    <!-- Datatables responsivo -->
    <link rel="stylesheet" href="assets/adminlte/plugins/datatables-rowreorder/css/rowReorder.bootstrap4.min.css">
    <link rel="stylesheet" href="assets/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="assets/eesc-ci4-theme/img/favicon.ico">
    <link rel="stylesheet" type="text/css" href="assets/eesc-ci4-theme/css/eesc-theme.css">

    <?=$this->renderSection('custom_css');?>

</head>

<body class="hold-transition layout-top-nav">

    <div class="wrapper">

        <!-- Navbar - Menu Principal -->
        <?=view('EESC_Theme\partials\userbar', ['theme' => $theme]);?>
        <?=view('EESC_Theme\partials\topbar', ['topbar' => $theme['topbar']]);?>
        <!-- /.navbar -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper mt-2">

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <?=view('EESC_Theme\partials\conteudo', ['theme' => $theme]);?>
                </div>
            </section>

        </div>
        <!-- /.content-wrapper -->

        <!-- Rodapé -->
        <?=view('EESC_Theme\partials\footer', ['theme' => $theme]);?>


    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="assets/adminlte/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="assets/adminlte/dist/js/adminlte.min.js"></script>

    <!-- Datatables -->
    <script src="assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript" src="assets/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="assets/adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>

    <!-- Datatables rowReorder -->
    <script type="text/javascript" src="assets/adminlte/plugins/datatables-rowreorder/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="assets/adminlte/plugins/datatables-rowreorder/js/rowReorder.bootstrap4.min.js"></script>

    <!-- Datatables responsive -->
    <script type="text/javascript" src="assets/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="assets/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    <script type="text/javascript" src="assets/adminlte/plugins/moment/moment-with-locales.min.js"></script>

    <script type="text/javascript" src="assets/eesc-ci4-theme/js/eesc-theme.js"></script>

    <?=$this->renderSection('custom_js');?>

</body>

</html>