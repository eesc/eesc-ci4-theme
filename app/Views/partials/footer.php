<?php $this->section('footer');?>

<footer class="main-footer text-center p-2 small">
    © <?=date('Y');?>
    <a href="<?=$theme['unidadeUSP']['url'];?>" target="_blank"><?=$theme['unidadeUSP']['nome'];?></a> /
    <a href="http://www.usp.br" target="_blank">Universidade de São Paulo</a>. Todos os direitos reservados.
    <a href="<?=site_url('creditos');?>">Créditos</a>.
</footer>

<?php $this->endsection('footer');?>

<?=$this->renderSection('footer');?>