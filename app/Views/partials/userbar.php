<nav class="navbar navbar-expand-sm navbar-light" style="z-index: 30;">
    <div class="container-fluid">

        <div class="d-none d-sm-block">
            <a class="logo-imagem" href="<?=site_url()?>">
                <img src="<?=$theme['unidadeUSP']['logo']?>" height="60px" alt="Logo da unidade" />
            </a>
        </div>

        <div class="d-none d-md-block mx-auto">
            <div class="h5 text-center ml-4">
                <?=$theme['appName']?><br>
                <small class="text-muted"><?=$theme['unidadeUSP']['nome']?></small>
            </div>
        </div>

        <div class="navbar-nav navbar-expand ml-auto">

            <?php if (empty($theme['user'])): // sem login ?>

	            <a class="btn btn-outline-primary btn-sm mt-2" href="<?=$theme['auth']['login_url'];?>">
	                Login <i class="fas fa-sign-in-alt"></i>
	            </a>

	            <?php else: // com login?>

	            <div class="btn-group mt-2" role="toolbar">

	                <a class="btn btn-outline-primary btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="100" href="#" role="button">
	                    <?=$theme['user']['nome'];?>
	                </a>
	                <?=view('EESC_Theme\partials\userbar-dropdown', ['theme' => $theme]);?>

	                <a class="btn btn-outline-primary btn-sm" href="<?=$theme['auth']['logout_url'];?>" role="button">
	                    Sair <i class="fas fa-sign-out-alt"></i>
	                </a>

	            </div>

	            <!-- Control Sidebar -->
	            <?php if ($theme['controlSidebar']): ?>
	            <a class="btn btn-sm text-secondary mt-2" title="Painel de controle" data-widget="control-sidebar" data-slide="true" href="#"
	                role="button">
	                <i class="fas fa-th-large"></i>
	            </a>
	            <?php endif?>

            <?php endif?>

        </div>
    </div>
</nav>

<!-- Control Sidebar -->
<?php if ($theme['controlSidebar']): ?>
<?=view('EESC_Theme\partials\control-sidebar');?>
<?php endif?>