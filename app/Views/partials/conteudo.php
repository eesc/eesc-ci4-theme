<?php
if (empty($this->sections['conteudo'])):
    $this->section('conteudo');?>

	<h2>Lorem Ipsum</h2>
	O Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão. O Lorem Ipsum tem vindo a ser o
	texto padrão usado por estas indústrias desde o ano de 1500, quando uma misturou os caracteres de um
	texto para criar um espécime de livro. Este texto não só sobreviveu 5 séculos, mas também o salto para a
	tipografia electrónica, mantendo-se essencialmente inalterada. <br>
	<br>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card card-outline card-primary">
	            <div class="card-header">

	                <h3 class="card-title form-inline">
	                    <b class="mr-2">Datatables em Card</b>
	                </h3>

	            </div>
	            <!-- /.card-header -->
	            <div class="card-body">
	                <table class="table table-stripped table-hover table-bordered datatable-default">
	                    <thead>
	                        <tr>
	                            <th>Nome</th>
	                            <th>Idade</th>
	                            <th>Tipo</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr>
	                            <td>John</td>
	                            <td>25</td>
	                            <td>User</td>
	                        </tr>
	                        <tr>
	                            <td>Jane</td>
	                            <td>32</td>
	                            <td>User</td>
	                        </tr>
	                        <tr>
	                            <td>Alice</td>
	                            <td>60</td>
	                            <td>Admin</td>
	                        </tr>
	                        <tr>
	                            <td>Maria</td>
	                            <td>48</td>
	                            <td>Admin</td>
	                        </tr>
	                    </tbody>
	                </table>
	            </div>
	            <!-- /.card-body -->
	        </div>
	    </div>
	</div>


	<?php $this->endSection();
endif;
$this->renderSection('conteudo');
?>