<?php foreach ($menu as $item):
    if (!empty($item['title'])) {
        $title = 'data-toggle="tooltip" data-placement="bottom" title="' . $item['title'] . '"';
    } else {
        $title = '';
    }
    ?>

	<li class="nav-item">
	    <a href="<?=$item['url']?>" class="nav-link" <?=$title?> ><?=$item['nome']?></a>
	</li>

<?php endforeach;?>