<?php $this->section('control-sidebar');?>

<aside class="control-sidebar control-sidebar-dark bg-navy" style="z-index: 20;" >
    <div class="p-3 control-sidebar-content">
        <h5 class="mt-3"><i class="fas fa-cogs"></i> Configurações</h5>
        <hr class="mb-3 border-light">
        <a href="#"><i class="fas fa-users-cog"></i> Administradores</a>
    </div>
</aside>

<?php $this->endsection('control-sidebar');?>

<?=$this->renderSection('control-sidebar');?>