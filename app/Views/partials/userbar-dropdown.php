<div class="dropdown-menu" style="width: 330px;">
    <!-- User image -->
    <div class="bg-navy text-center p-2">

        <?php if ($theme['user']['foto']): ?>
        <img src="data:image/png;base64, <?=$theme['user']['foto'];?>" class="img-circle elevation-2" alt="Foto do Usuário"><br>
        <?php endif?>

        <div><?=$theme['user']['nome'];?></div>
        <div class="small"><?=$theme['user']['codpes'];?></div>
        <div class="small"><?=$theme['user']['email'];?></div>
        <div class="small"><?=$theme['user']['ramal'];?></div>

    </div>

    <!-- Menu Body -->
    <div class="user-body p-2">
        <div class="row">
            <div class="col-6 text-center">
                <a href="<?=$theme['unidadeUSP']['sistemas_url'];?>" target="_blank">
                <?=$theme['unidadeUSP']['sistemas_menu'];?> <i class="fas fa-share"></i>
                </a>
            </div>
            <div class="col-6 text-center">
                <a href="https://uspdigital.usp.br" target="_blank">
                    Sistemas USP <i class="fas fa-share"></i>
                </a>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- Menu Footer-->
    <div class="user-footer p-2">
        <a href="<?=$theme['auth']['logout_url'];?>" class="btn btn-sm btn-outline-primary float-right">
            Sair <i class="fas fa-sign-out-alt"></i>
        </a>
    </div>
</div>