  <nav style="background-color: #e3f2fd; z-index: 10;" class="border-top main-header navbar navbar-expand-sm navbar-light py-0">
      <div class="container-fluid">

          <a href="#" class="navbar-brand">
              <?=$theme['appShortName']?>
          </a>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
              aria-expanded="false" aria-label="Toggle navigation">
              <i class="fas fa-bars fa-1"></i>
          </button>

          <div class="collapse navbar-collapse" id="navbarCollapse">
              <?=view('EESC_Theme\\partials\topbar-left', ['menu' => $topbar['left']]);?>
              <?=view('EESC_Theme\\partials\topbar-right', ['menu' => $topbar['right']]);?>
          </div>

      </div>
  </nav>