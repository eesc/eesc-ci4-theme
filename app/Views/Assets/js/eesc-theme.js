// Inicalizando datatables defaults
$.extend(true, $.fn.dataTable.defaults, {
    language: {
        url: 'assets/datatables-i18n/i18n/pt-BR.json'
    },
    responsive: true,
    //rowReorder: {
        //selector: 'td:nth-child(2)'
    //},
});

// Inicializando moment defaults
moment.locale('pt-BR')

$(document).ready(function () {

    // inicializando datatables global
    oTable = $('.datatable-default').dataTable()

    // inicializando tooltip global
    $('[data-toggle="tooltip"]').tooltip()

})