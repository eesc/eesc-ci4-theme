# EESC-CI4-THEME

Tema para sistemas da EESC para uso com Codeigniter 4.

* Possui uma barra de usuário
* Possui barra de menu da aplicação configurável
* Baseado no adminlte 3


![theme image](docs/tela-principal.png)

## Dependências

Este tema depende de `oomphinc/composer-installers-extender` para copiar os assets para o local adequado.

Depende de `bower-asset/adminlte`


## Instalação

### Novo projeto

	composer create-project codeigniter4/appstarter project-root

Seguir instruções do projeto existente

### Projeto existente

Edite o arquivo `composer.json` acrescentando as linhas a seguir conforme necessário:

	"require": {
		"oomphinc/composer-installers-extender": "^1.1",
        "eesc/eesc-ci4-theme": "dev-master",
        ...

	"repositories": [
		{
			"type": "composer",
			"url": "https://asset-packagist.org"
		},
		{
			"type": "vcs",
			"url": "https://gitlab.uspdigital.usp.br/eesc/eesc-ci4-theme"
		}
	],
	"extra": {
		"installer-types": [
			"bower-asset",
			"npm-asset"
		],
		"installer-paths": {
			"public/assets/{$name}/": [
				"type:bower-asset",
				"type:npm-asset"
			]
		}
	},
	"scripts": {
		"post-autoload-dump": [
			"EESC\\Theme\\ComposerScripts::post_autoload_dump"
		],
		"post-root-package-install": [
		],
		"post-create-project-cmd": [
		]
	},
	"minimum-stability": "dev",
	"prefer-stable": true

Rode `composer update` ou `composer install`

## Configuração

post-autoload-dump irá:

* adicionar em `app/Config/Autoload.php` 'EESC_Theme' => ROOTPATH.'vendor/eesc/eesc-ci4-theme/app',
* adicionar `app/Config/Eesc_ci4_theme.php` o arquivo de configuração correspondente
* publicar os assets


Edite o arquivo `app/Config/Eesc_ci4_theme.php` conforme necessário.

Opcionalmente você pode desativar o control-sidebar.


Nas views da aplicação estenda o template do pacote:

    $this->extend('EESC_Theme\\template-topbar');


Configurar o .env 

* app.baseURL = 'caminho da aplicação'

ou php spark serve