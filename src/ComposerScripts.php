<?php namespace EESC\Theme;

class ComposerScripts
{
    public static function post_autoload_dump($event)
    {
        SELF::publishPSR4($event);
        SELF::publishConfig($event);
        SELF::publishAssets($event);
    }

    public static function publishPSR4($event)
    {
        $vendor_dir = $event->getComposer()->getConfig()->get('vendor-dir');

        $src_str = "		'EESC_Theme' => ROOTPATH . 'vendor/eesc/eesc-ci4-theme/app'," . PHP_EOL;
        $after_str = "'Config'=>APPPATH.'Config',";
        $file = $vendor_dir . '/../app/Config/Autoload.php';
        $dest_arr = file($file);

        // vamos ver se já foi publicado
        foreach ($dest_arr as $line) {
            if (preg_replace('/\s+/', '', $src_str) == preg_replace('/\s+/', '', $line)) {
                // encontrou $insert, abortando
                echo 'Eesc-ci4-theme: Autoload ok: nada a fazer' . PHP_EOL;
                return true;
            }
        }

        // vamos publicar
        for ($i = 0; $i < count($dest_arr); $i++) {
            if ($after_str == preg_replace('/\s+/', '', $dest_arr[$i])) {
                array_splice($dest_arr, $i, 0, $src_str);
                file_put_contents($file, implode('', $dest_arr));
                echo 'Eesc-ci4-theme: Autoload publicado com sucesso' . PHP_EOL;
                return true;
            }
        }
    }

    public static function publishAssets($event)
    {
        $vendor_dir = $event->getComposer()->getConfig()->get('vendor-dir');

        $src = __DIR__ . '/../app/Views/Assets';
        $dst = $vendor_dir . '/../public/assets/eesc-ci4-theme';

        SELF::recurse_copy($src, $dst);
        echo 'Eesc-ci4-theme: Assets publicados' . PHP_EOL;
        return true;
    }

    public static function publishConfig($event)
    {
        $arq = 'Eesc_ci4_theme.php';

        $vendor_dir = $event->getComposer()->getConfig()->get('vendor-dir');
        $src = __DIR__ . '/../app/Config/' . $arq;
        $dst = $vendor_dir . '/../app/Config/' . $arq;

        if (!file_exists($dst)) {
            copy($src, $dst);
            echo 'Eesc-ci4-theme: Config publicado => ' . realpath($dst) . PHP_EOL;
        } else {
            echo 'Eesc-ci4-theme: Config existente: verifique se houve atualização' . PHP_EOL;
        }
        return true;
    }

    protected static function recurse_copy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    SELF::recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}
